﻿using System.Collections.Generic;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Controllers
{
    public class AboutController : Controller
    {

        public ActionResult Index()
        {
            ImageAutoDao autoDao = new ImageAutoDao();
            IList<ImageAuto> auta = autoDao.GetAll();

            return View(auta);
        }

    }
}