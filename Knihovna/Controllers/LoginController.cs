﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataAccess.Dao;
using DataAccess.Model;
using Knihovna.Class;
using Microsoft.AspNet.Identity;

namespace Knihovna.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {



            return View();
        }

        public ActionResult SignIn(string login, string password)
        {
            if (Membership.ValidateUser(login, password))
            {
                RoleProvider role = new ProdejnaRoleProvider();
                FormsAuthentication.SetAuthCookie(login, false);
                if (role.IsUserInRole(login, "Admin"))
                {
                    return RedirectToAction("Index", "Home", new {Area = "Admin"});
                }

                return RedirectToAction("Index", "Home");
            }

            TempData["error"] = "Login nebo password neni spravny";
            return RedirectToAction("Index", "Login");

        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult Detail()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ImageUserDao imageUserDao = new ImageUserDao();

            ZakaznikRegistrovanyDao zakanzikDao = new ZakaznikRegistrovanyDao();
            ZakaznikRegistrovany zakaznikReg = zakanzikDao.GetByZakaznik(zakaznik.Id);
            if (zakaznikReg == null)
            {
                zakaznikReg = new ZakaznikRegistrovany();
            }

            ViewBag.Pixels = zakaznikReg;

            Zakaznik zakaz = zakaznikDao.GetByLogin(User.Identity.Name);
            ImageUser imageUser = imageUserDao.GetByZakaznikId(zakaz.Id) ?? new ImageUser();

            ViewBag.Foto = imageUser.ImageName;

            return View(zakaznik);
        }


        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SignUp(Zakaznik zakaznik)
        {



            if (ModelState.IsValid)
            {

                OpravneniDao opravneni = new OpravneniDao();
                IList<Opravneni> list = opravneni.GetAll();

                zakaznik.Opravneni = list[1];
                ZakaznikDao zakaznikDao = new ZakaznikDao();
                IList<Zakaznik> z = zakaznikDao.GetListByLogin(zakaznik.Email);
                if (z.Count == 0)
                {

                    MestoDao mestoDao = new MestoDao();
                    IList<Mesto> me = mestoDao.GetAll();

                    int pocet = 0;
                    foreach (var c in me)
                    {
                        pocet = c.Id;
                    }


                    Mesto mesto = new Mesto
                    {
                        Psc = zakaznik.Psc.Psc,
                        Id = pocet + 1
                    };
                    mestoDao.Create(mesto);
                        zakaznik.Psc.Id = mesto.Id;
                    
                    string pass = new CustomPasswordHasher().HashPassword(zakaznik.Password);
                    zakaznik.Password = pass;
                    zakaznikDao.Create(zakaznik);
                       
                    if (new ProdejnaMembershipProvider().ValidateUserWithout(zakaznik.Email, zakaznik.Password))
                    {                       
                        FormsAuthentication.SetAuthCookie(zakaznik.Email, false);
                        return RedirectToAction("Index", "Home");
                    }

                }

            }
            else
            {
                return View("SignUp");
            }




            TempData["error"] = "Login nebo password neni spravny nebo se jiz nachazi v databazi";
            return View("SignUp");

        }

        [HttpPost]
        public ActionResult Remember(string Email, string Password, string Jmeno, string Prijmeni)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(Email);
            if (zakaznik == null)
            {
                TempData["error"] = "Nepodarilo se vas najit";

                return View();

            }
            else
            {

                if (zakaznik.Jmeno == Jmeno && zakaznik.Prijmeni == Prijmeni)
                {                
                    zakaznik.Password = new CustomPasswordHasher().HashPassword(Password);
                    zakaznikDao.Update(zakaznik);
                    return RedirectToAction("Index", "Home");

                }

                TempData["error"] = "Nepodarilo se";
                return RedirectToAction("Index", "Home");
            }


        }

        [HttpGet]
        public ActionResult Remember()
        {
            return View();

        }
        [Authorize]
        public ActionResult Fotog()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ViewBag.Zakaznik = zakaznik;
            ImageUserDao imageDao = new ImageUserDao();
            ImageUser image = imageDao.GetByZakaznikId(zakaznik.Id);
            if (image == null)
            {
                image = new ImageUser();
            }


            if (Request.IsAjaxRequest())
            {

                return PartialView(image);
            }

            return View(image);
        }
        [Authorize]
        public ActionResult Infos()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {

                return PartialView(zakaznik);
            }

            return View(zakaznik);
        }
        [Authorize]
        public ActionResult Heslo()
        {
            if (Request.IsAjaxRequest())
            {

                return PartialView();
            }

            return View();
        }



        [Authorize]
        public ActionResult UpdateO(HttpPostedFileBase img)
        {
            try
            {
                ZakaznikDao zakaznikDao = new ZakaznikDao();
                Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

                ImageUserDao imageDao = new ImageUserDao();
                ImageUser image = imageDao.GetByZakaznikId(zakaznik.Id);

                if (img != null)
                {
                    if (img.ContentType == "image/jpeg" || img.ContentType == "image/png")
                    {
                        Guid gu = Guid.NewGuid();
                        string imageName = gu.ToString() + ".jpg";
                        img.SaveAs(Server.MapPath("~/img/img_users/" + imageName));
                        if (image == null)
                        {
                            image = new ImageUser();
                            image.ImageName = imageName;
                            image.Zakaznik = zakaznik;
                            imageDao.Create(image);

                        }
                        else
                        {
                            image.ImageName = imageName;
                            imageDao.Update(image);

                        }

                        TempData["message-success"] = "Zákazník byl upraven";


                    }
                }

            }
            catch (Exception e)
            {
                TempData["error"] = "chyba";
                throw;
            }

           


            return RedirectToAction("Detail");
        }
        [Authorize]
        public ActionResult UpdateI(Zakaznik zakaznik)
        {
            try
            {

                ZakaznikDao zakaznikDao = new ZakaznikDao();
                zakaznikDao.Update(zakaznik);

                TempData["message-success"] = "Zákazník byl upraven";
            }
            catch (Exception e)
            {
                TempData["error"] = "chyba";

                throw;

            }



            return RedirectToAction("Detail");
        }
        [Authorize]
        public ActionResult UpdateH(string PasswordOld, string PasswordNew)
        {

            MembershipProvider membership = new ProdejnaMembershipProvider();

           
            if (membership.ChangePassword(User.Identity.Name, PasswordOld, PasswordNew))
            {
              
                TempData["message-success"] = "Heslo bylo změněno";

            }
            else
            {
                TempData["error"] = "Heslo nebylo změněno";

            }

            return RedirectToAction("Detail");
        }
        [Authorize]
        public ActionResult Vip()
        {

            if (Request.IsAjaxRequest())
            {

                return PartialView();
            }

            return View();



        }

        public ActionResult UpdateV(Code code)
        {

            if (code == null)
            {
                code = new Code();
            }
            CodeDao codeDao = new CodeDao();

            Code value = codeDao.GetByCode(code.Scode);
            if (value != null)
            {
                ZakaznikDao zakaznikDao = new ZakaznikDao();
                Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

                OpravneniDao opravneniDao = new OpravneniDao();
                Opravneni opravneni = opravneniDao.GetById(3);

                zakaznik.Opravneni = opravneni;
                zakaznikDao.Update(zakaznik);
                TempData["message-success"] = "VIP změněno";

            }
            else
            {
                TempData["error"] = "chyba";

            }
            return RedirectToAction("Detail");
        }
        [Authorize]
        public ActionResult Testovaci()
        {

            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ZakaznikRegistrovanyDao registrovanyDao = new ZakaznikRegistrovanyDao();
            ZakaznikRegistrovany registrovany = registrovanyDao.GetByZakaznik(zakaznik.Id);
            if (registrovany == null)
            {registrovany = new ZakaznikRegistrovany();
            }



            TestovaciJizdaDao testovaciJizdaDao = new TestovaciJizdaDao();
            IList<TestovaciJizda> test = testovaciJizdaDao.GetListByRegistrovany(registrovany.Id);



            return View(test);
        }
        [Authorize]
        public ActionResult Objednavky()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

            ObjednavkaDao objednavkaDao = new ObjednavkaDao();
            IList<Objednavka> objednavka = objednavkaDao.GetListByZakaznik(zakaznik.Id);



            return View(objednavka);
        }
    }
}