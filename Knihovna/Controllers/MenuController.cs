﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Controllers
{
    public class MenuController : Controller
    {
        [ChildActionOnly]
        // GET: Admin/Menu
        public ActionResult Index()
        {
            ZakaznikDao UserDao = new ZakaznikDao();
            Zakaznik Us = UserDao.GetByLogin(User.Identity.Name);

            return View(Us);
        }

        public ActionResult Carousel()
        {

            SystemsDao sysDao = new SystemsDao();


            IList<Systems> sys = sysDao.GetByMarker(10);

            return View(sys);
        }
    }
}