﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Mvc;
using Knihovna.Class;
using SendGrid;
using SendGrid.Helpers.Mail;


namespace Knihovna.Controllers
{
    public class ContactController : Controller
    {

        public ActionResult Index()
        {


            return View();
        }

        public ActionResult Mail(string name, string mail ,string content)
        {
            string subject = "Kontaktni formular na webu ProdejnaPixel od uzivatele " + name;

            try
            {
                if ((!string.IsNullOrEmpty(name))&& (!string.IsNullOrEmpty(mail))&& (!string.IsNullOrEmpty(content)))
                {
                    Mailer.OnlyPostMail(content, name, mail, subject);
                    TempData["message-success"] = "Email byl odeslán";
                }
                else
                {
                    TempData["error"] = "Chyba";

                }



            }
            catch (Exception)
            {
                throw;
            }


            return RedirectToAction("Index");

        }
    }
}