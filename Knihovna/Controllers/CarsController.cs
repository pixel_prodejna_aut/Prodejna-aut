﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Controllers
{
    public class CarsController : Controller
    {

        public ActionResult Index(int? page)
        {
            int itemsOnPage = 5;
            int pg = page.HasValue ? page.Value : 1;
            int totalCars;


            AutoDao autoDao = new AutoDao();
            IList<Auto> auta = autoDao.GetCarsPaged(itemsOnPage,pg,out totalCars);


            ViewBag.Pages = (int) Math.Ceiling((double) totalCars / (double) itemsOnPage);
            ViewBag.CurrentPage = pg;

            ImageAutoDao ImageDao = new ImageAutoDao();
            IList<ImageAuto> image = ImageDao.GetAll();

            ViewBag.Photo = image;
            return View(auta);
        }

        public ActionResult Detail(int id)
        {
            AutoDao autoDao = new AutoDao();
            Auto a = autoDao.GetById(id);


            ImageAutoDao ImageDao = new ImageAutoDao();
            IList<ImageAuto> image = ImageDao.ImageAuto(id);
            ViewBag.Photo = image;
            return View(a);
        }


        public ActionResult Search(string phrase)
        {
            AutoDao autoDao = new AutoDao();
            IList<Auto> auta = autoDao.SearchCars(phrase);
            
            return View("Index", auta);
        }

        public ActionResult ImageRender(int id)
        {

            ImageAutoDao imageAutoDao = new ImageAutoDao();

            IList<ImageAuto> imageAuto = imageAutoDao.HeadImageAuto(id);

            if (imageAuto.Count == 0)
            {
                imageAuto = imageAutoDao.HeadImageAuto(1);
            }

            return View(imageAuto);
        }
        [Authorize]
        public ActionResult Testovat(int id)
        {

            


            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ViewBag.Zakazanik = zakaznik;

            AutoDao autoDao = new AutoDao();
            Auto a = autoDao.GetById(id);
            ImageAutoDao imageDao = new ImageAutoDao();
            IList<ImageAuto> image = imageDao.HeadImageAuto(id);
            ViewBag.Image = image;


            ZakaznikRegistrovanyDao registrovanyDao = new ZakaznikRegistrovanyDao();
            ZakaznikRegistrovany registrovany = registrovanyDao.GetByZakaznik(zakaznik.Id);
            if (registrovany == null)
            {registrovany = new ZakaznikRegistrovany();
            }

            ViewBag.tel = registrovany.Telefon;
            ViewBag.ulice = registrovany.Ulice;
            ViewBag.mesto = registrovany.Mesto;

            return View(a);

        }
        [Authorize]
        public ActionResult Objednat(int id)
        {

            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ViewBag.Zakazanik = zakaznik;


            AutoDao autoDao = new AutoDao();
            Auto a = autoDao.GetById(id);
            ImageAutoDao imageDao = new ImageAutoDao();
            IList<ImageAuto> image = imageDao.HeadImageAuto(id);
            ViewBag.Image = image;

            return View(a);

        }
        [Authorize]
        public ActionResult ObjednatAuto(int id)
        {
            DateTime date = DateTime.Now;

            AutoDao autoDao = new AutoDao();
            Auto auto = autoDao.GetById(id);

            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

            ZamestnanecDao zamestnanecDao = new ZamestnanecDao();
            IList<Zamestnanec> zamestnanec = zamestnanecDao.GetByPobocka(auto.Pobocka.Id);
            Zamestnanec obchodnik = zamestnanec[new Random().Next(zamestnanec.Count)];

            PrislusenstviDao prislusenstviDao = new PrislusenstviDao();
            IList<Prislusenstvi> prislusenstvi = prislusenstviDao.GetAll();
            Prislusenstvi prislus = prislusenstvi[new Random().Next(prislusenstvi.Count)];

            ObjednavkaDao objednavkaDao = new ObjednavkaDao();
            Objednavka objednavka = new Objednavka();

            objednavka.Auto = auto;
            objednavka.Zakaznik = zakaznik;
            objednavka.Datum = date;
            objednavka.Zamestnanec = obchodnik;
            objednavka.Prislusenstvi = prislus;

            objednavkaDao.Create(objednavka);
            TempData["message-success"] = "Objednávka byla dokončena";

            return RedirectToAction("Index");
        }
        public ActionResult TestovatAuto(string telefon, string ulice, string mesto,int id)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

            ZakaznikRegistrovanyDao registrovanyDao = new ZakaznikRegistrovanyDao();
            ZakaznikRegistrovany registrovany = registrovanyDao.GetByZakaznik(zakaznik.Id);
            if (registrovany == null)
            {
                registrovany = new ZakaznikRegistrovany();
                registrovany.Zakaznik = zakaznik;
                registrovany.Telefon = telefon;
                registrovany.Ulice = ulice;
                registrovany.Mesto = mesto;
                registrovany.PocetPixelFreeBodu = 0;

                registrovanyDao.Create(registrovany);
            }
            else
            {
                registrovany.Telefon = telefon;
                registrovany.Ulice = ulice;
                registrovany.Mesto = mesto;
                registrovany.PocetPixelFreeBodu = registrovany.PocetPixelFreeBodu + 10;
                registrovanyDao.Update(registrovany);

            }


            AutoDao autoDao = new AutoDao();
            Auto auto = autoDao.GetById(id);
            TestovaciJizdaDao testovaciDao = new TestovaciJizdaDao();
            TestovaciJizda testovaciJizda = new TestovaciJizda();


            ZamestnanecDao zamestnanecDao = new ZamestnanecDao();
            IList<Zamestnanec> zamestnanec = zamestnanecDao.GetByPobocka(auto.Pobocka.Id);
            Zamestnanec obchodnik = zamestnanec[new Random().Next(zamestnanec.Count)];




            testovaciJizda.Auto = auto;
            testovaciJizda.Datum = DateTime.Now;
            testovaciJizda.Zamestnanec = obchodnik;
            testovaciJizda.ZakaznikRegistrovany = registrovany;
            testovaciJizda.Delka = 10;
            testovaciJizda.Popis = "platna";
            testovaciDao.Create(testovaciJizda);

            TempData["message-success"] = "Objednávka byla dokončena";
            return RedirectToAction("Index");
        }
    }
}