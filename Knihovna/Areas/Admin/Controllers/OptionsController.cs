﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using DataAccess.Dao;
using DataAccess.Model;
using Systems = DataAccess.Model.Systems;

namespace Knihovna.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OptionsController : Controller
    {
        // GET: Admin/Options
        public ActionResult Index()
        {

            CodeDao codeDao = new CodeDao();
            IList<Code> code = codeDao.GetAll();
            ViewBag.Code = code;

            SystemsDao sysDao = new SystemsDao();

            IList<Systems> sys = sysDao.GetByMarker(10);
            return View(sys);
        }

        public ActionResult Pohled(int id)
        {
            SystemsDao sysDao = new SystemsDao();
            Systems sys = sysDao.GetById(id);

            return View(sys);
        }


        
        public ActionResult UpdateO(Systems systems, HttpPostedFileBase obr1)
        {
            SystemsDao sysDao = new SystemsDao();
            Systems sys = sysDao.GetById(systems.Id);

            try
            {
                if (obr1 != null)
                {
                    if (obr1.ContentType == "image/jpeg" || obr1.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(obr1.InputStream);
                        Guid guid = Guid.NewGuid();
                        string imageName = guid.ToString() + ".jpg";
                        obr1.SaveAs(Server.MapPath("~/img/" + imageName));
                        System.IO.File.Delete(Server.MapPath("~/img/" + sys.Value));
                        systems.Value = imageName;
                    }
                }
                sysDao.Merge(systems);
                TempData["message-success"] = "Záznam byl upraven";
            }
            catch (Exception)
            {
                throw;
            }
          
            return RedirectToAction("Index");

        }

        public ActionResult Delete(int id)
        {
            try
            {
                CodeDao codeDao = new CodeDao();
                Code code = codeDao.GetById(id);

                codeDao.Delete(code);
                TempData["message-success"] = "Kód byl smazán";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        public ActionResult AddCode()
        {
            CodeDao codeDao = new CodeDao();



            Code code = new Code();
            code.Scode = Guid.NewGuid().ToString();

            codeDao.Create(code);


            return RedirectToAction("Index");
        }




    }
}