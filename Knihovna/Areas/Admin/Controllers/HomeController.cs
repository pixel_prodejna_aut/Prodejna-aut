﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Areas.Admin.Controllers
{
   
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {

            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                return View();
            }

        
            return RedirectToAction("Index", "Login", new {area=""});
        }

        


        [Authorize(Roles = "Admin")]
        public ActionResult Main()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            return RedirectToAction("Index", "Home", new { area = "" });
        }
    }
}