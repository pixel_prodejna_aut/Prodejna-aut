﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;
using Region = DataAccess.Model.Region;

namespace Knihovna.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DatabaseController : Controller
    {
        // GET: Admin/Database
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Objednavky()
        {
            ObjednavkaDao objednavkaDao = new ObjednavkaDao();
            IList<Objednavka> objednavka = objednavkaDao.GetAll();

            return View(objednavka);
        }
        public ActionResult Testovaci()
        {
            TestovaciJizdaDao testovaciDao = new TestovaciJizdaDao();
            IList<TestovaciJizda> testovaci = testovaciDao.GetAll();

            return View(testovaci);
        }
        public ActionResult Pobocky()
        {
            PobockaDao pobockaDao = new PobockaDao();
            IList<Pobocka> pobocka = pobockaDao.GetAll();

            return View(pobocka);
        }
        public ActionResult Zakaznici()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            IList<Zakaznik> zakaznik = zakaznikDao.GetAll();


            return View(zakaznik);
        }
        public ActionResult Auta()
        {
            AutoDao autoDao = new AutoDao();
            IList<Auto> auto = autoDao.GetAll();

            ImageAutoDao imageDao = new ImageAutoDao();

            return View(auto);
        }

        public ActionResult Prislusenstvi()
        {

            PrislusenstviDao Dao = new PrislusenstviDao();
            IList<Prislusenstvi> prislusenstvi = Dao.GetAll();

            return View(prislusenstvi);
        }

        public ActionResult ImageRender(int id)
        {

            ImageAutoDao imageAutoDao = new ImageAutoDao();

            IList<ImageAuto> imageAuto = imageAutoDao.HeadImageAuto(id);

            if (imageAuto.Count == 0)
            {
                imageAuto = imageAutoDao.HeadImageAuto(1);
            }

            return View(imageAuto);
        }



        //Objednavka Controllers
        public ActionResult EditO(int id)
        {
            ObjednavkaDao Dao = new ObjednavkaDao();
            Objednavka objednavka = Dao.GetById(id);

            ZamestnanecDao zamestnanecDao = new ZamestnanecDao();
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            PrislusenstviDao prislusenstviDao = new PrislusenstviDao();
            AutoDao autoDao = new AutoDao();



            ViewBag.Zam = zamestnanecDao.GetAll();
            ViewBag.Zak = zakaznikDao.GetAll();
            ViewBag.Pri = prislusenstviDao.GetAll();
            ViewBag.Auto = autoDao.GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView(objednavka);

            }

            return View(objednavka);
        }

        public ActionResult DeleteO(int id)
        {
            try
            {
                ObjednavkaDao Dao = new ObjednavkaDao();
                Objednavka objednavka = Dao.GetById(id);
                Dao.Delete(objednavka);
                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception exception)
            {
                throw;
            }
            return RedirectToAction("Objednavky");
        }

        [HttpPost]
        public ActionResult UpdateO(Objednavka objednavka)
        {

            try
            {
                ObjednavkaDao objednavkaDao = new ObjednavkaDao();


                objednavkaDao.Update(objednavka);

                TempData["message-success"] = "Objednávka byla upravena";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Objednavky");
        }

        public ActionResult DetailO(int id)
        {
            ObjednavkaDao objednavkaDao = new ObjednavkaDao();
            Objednavka objednavka = objednavkaDao.GetById(id);

            return PartialView(objednavka);
        }
        //------------------------------


        //TODO
        // Testovaci jizdy controller
        public ActionResult EditT(int id)

        {

            AutoDao autDao = new AutoDao();
            ZakaznikRegistrovanyDao regDao = new ZakaznikRegistrovanyDao();
            ZamestnanecDao zamDao = new ZamestnanecDao();


            ViewBag.Zam = zamDao.GetAll();
            ViewBag.Reg = regDao.GetAll();
            ViewBag.Aut = autDao.GetAll();


            TestovaciJizdaDao Dao = new TestovaciJizdaDao();
            TestovaciJizda testovaci = Dao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(testovaci);
            }



            return View(testovaci);
        }

        public ActionResult DeleteT(int id)
        {
            try
            {
                TestovaciJizdaDao Dao = new TestovaciJizdaDao();
                TestovaciJizda testovaci = Dao.GetById(id);
                Dao.Delete(testovaci);
                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception exception)
            {
                throw;
            }
            return RedirectToAction("Testovaci");
        }

        [HttpPost]
        public ActionResult UpdateT(TestovaciJizda jizda)
        {

            try
            {
                TestovaciJizdaDao Dao = new TestovaciJizdaDao();

                Dao.Update(jizda);

                TempData["message-success"] = "Jízda byla upravena";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Testovaci");
        }

        public ActionResult DetailT(int id)
        {
            TestovaciJizdaDao Dao = new TestovaciJizdaDao();
            TestovaciJizda testovaci = Dao.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(testovaci);

            }

            return View(testovaci);
        }
        //------------------------------

        //TODO
        //  prehled pobocek controller
        public ActionResult EditP(int id)
        {

            PobockaDao Dao = new PobockaDao();
            Pobocka pobocka = Dao.GetById(id);


            RegionDao regionDao = new RegionDao();
            ViewBag.Region = regionDao.GetAll();


            if (Request.IsAjaxRequest())
            {
                return PartialView(pobocka);

            }

            return View(pobocka);
        }

        public ActionResult DeleteP(int id)
        {
            try
            {
                PobockaDao Dao = new PobockaDao();
                Pobocka pobocka = Dao.GetById(id);

                Dao.Delete(pobocka);
                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception exception)
            {
                throw;
            }
            return RedirectToAction("Pobocky");
        }

        [HttpPost]
        public ActionResult UpdateP(Pobocka pobocka)
        {

            try
            {
                PobockaDao pobockaDao = new PobockaDao();


                pobockaDao.Update(pobocka);

                TempData["message-success"] = "Pobočka byla upravena";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Pobocky");
        }

        public ActionResult DetailP(int id)
        {
            PobockaDao Dao = new PobockaDao();
            Pobocka pobocka = Dao.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(pobocka);
            }
            return View(pobocka);
        }




        public ActionResult AddP()
        {
            Pobocka pobocka = new Pobocka();
            RegionDao regionDao = new RegionDao();
            IList<Region> region = regionDao.GetAll();
            ViewBag.Region = region;

            if (Request.IsAjaxRequest())
            {
                return PartialView(pobocka);
            }

            return View(pobocka);
        }

        public ActionResult AddPobocka(Pobocka pobocka)
        {
            try
            {
                PobockaDao Dao = new PobockaDao();
                Dao.Create(pobocka);

            }
            catch (Exception)
            {
                throw;
            }


            return RedirectToAction("Pobocky");
        }

        //------------------------------


        // Zakaznici controller
        public ActionResult EditZ(int id)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetById(id);

            OpravneniDao opravneni = new OpravneniDao();
            ViewBag.Opravneni = opravneni.GetAll();
            if (Request.IsAjaxRequest())
            {
                return PartialView(zakaznik);

            }

            return View(zakaznik);
        }

        public ActionResult DeleteZ(int id)
        {
            try
            {
                ZakaznikDao Dao = new ZakaznikDao();
                Zakaznik zakaznik = Dao.GetById(id);

                Dao.Delete(zakaznik);
                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception exception)
            {
                throw;
            }
            return RedirectToAction("Zakaznici");
        }

        [HttpPost]
        public ActionResult UpdateZ(Zakaznik zakaznik)
        {

            try
            {

                ZakaznikDao zakaznikDao = new ZakaznikDao();

                zakaznikDao.Update(zakaznik);

                TempData["message-success"] = "Zákazník byl upraven";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Zakaznici");
        }

        public ActionResult DetailZ(int id)
        {
            ZakaznikDao Dao = new ZakaznikDao();
            Zakaznik zakaznik = Dao.GetById(id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(zakaznik);

            }

            return View(zakaznik);
        }
        //------------------------------




        //editace auto controller 
        public ActionResult EditA(int id)
        {


            ZemeDao zemeDao = new ZemeDao();
            FakturaDao fakturaDao = new FakturaDao();
            PobockaDao pobockaDao = new PobockaDao();
            ZnackaDao znackaDao = new ZnackaDao();



            ViewBag.Zeme = zemeDao.GetAll();
            ViewBag.Faktura = fakturaDao.GetAll();
            ViewBag.Pobocka = pobockaDao.GetAll();
            ViewBag.Znacka = znackaDao.GetAll();


            AutoDao autoDao = new AutoDao();
            Auto auto = autoDao.GetById(id);


            if (Request.IsAjaxRequest())
            {
                return PartialView(auto);

            }

            return View(auto);
        }

        public ActionResult DeleteA(int id)
        {
            try
            {

                AutoDao Dao = new AutoDao();
                Auto auto = Dao.GetById(id);

                Dao.Delete(auto);
                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception exception)
            {
                throw;
            }
            return RedirectToAction("Auta");
        }

        [HttpPost]
        public ActionResult UpdateA(Auto auto)
        {




            try
            {
                AutoDao autoDao = new AutoDao();



                autoDao.Update(auto);

                TempData["message-success"] = "Auto bylo smazáno";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Auta");
        }

        public ActionResult DetailA(int id)
        {
            AutoDao Dao = new AutoDao();
            Auto auto = Dao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(auto);
            }

            return View(auto);
        }

        public ActionResult AddA()
        {

            ZemeDao zemeDao = new ZemeDao();
            FakturaDao fakturaDao = new FakturaDao();
            PobockaDao pobockaDao = new PobockaDao();
            ZnackaDao znackaDao = new ZnackaDao();



            ViewBag.Zeme = zemeDao.GetAll();
            ViewBag.Faktura = fakturaDao.GetAll();
            ViewBag.Pobocka = pobockaDao.GetAll();
            ViewBag.Znacka = znackaDao.GetAll();




            Auto auto = new Auto();

            if (Request.IsAjaxRequest())
            {

                return PartialView(auto);
            }


            return View(auto);

        }

        public ActionResult AddAuto(Auto auto)
        {
            try
            {
                AutoDao autoDao = new AutoDao();

                autoDao.Create(auto);

            }
            catch (Exception)
            {
                throw;
            }


            return RedirectToAction("Auta");
        }
        //------------------------------


        // controller pro prislusenstvi
        public ActionResult EditPr(int id)
        {

            PrislusenstviDao prislusenstviDao = new PrislusenstviDao();
            Prislusenstvi prislusenstvi = prislusenstviDao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(prislusenstvi);

            }

            return View(prislusenstvi);

        }

        public ActionResult DeletePr(int id)
        {


            try
            {
                PrislusenstviDao prislusenstviDao = new PrislusenstviDao();
                Prislusenstvi prislusenstvi = prislusenstviDao.GetById(id);
                prislusenstviDao.Delete(prislusenstvi);

                TempData["message-success"] = "Položka byla smazana";
            }
            catch (Exception)
            {

                throw;
            }


            return RedirectToAction("Prislusenstvi");

        }

        public ActionResult DetailPr(int id)
        {
            PrislusenstviDao prislusenstviDao = new PrislusenstviDao();
            Prislusenstvi prislusenstvi = prislusenstviDao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(prislusenstvi);
            }

            return View(prislusenstvi);

        }

        //-----------------------------------

        public ActionResult UpdatePr(Prislusenstvi prislusenstvi)
        {
            try
            {
                PrislusenstviDao Dao = new PrislusenstviDao();

                Dao.Update(prislusenstvi);

                TempData["message-success"] = "Příslušenství bylo smazáno";
            }
            catch (Exception)
            {
                throw;

            }

            return RedirectToAction("Prislusenstvi");
        }

        public ActionResult AddPr()
        {

            Prislusenstvi prislusenstvi = new Prislusenstvi();

            if (Request.IsAjaxRequest())
            {
                return PartialView(prislusenstvi);
            }

            return View(prislusenstvi);
        }

        public ActionResult AddPrislusenstvi(Prislusenstvi prislusenstvi)
        {
            try
            {
                PrislusenstviDao Dao = new PrislusenstviDao();
                Dao.Create(prislusenstvi);

            }
            catch (Exception)
            {
                throw;
            }



            return RedirectToAction("Prislusenstvi");
        }


        public ActionResult Zamestnanci()
        {
            ZamestnanecDao zamestnanecDao = new ZamestnanecDao();
            IList<Zamestnanec> zamestnanec = zamestnanecDao.GetAll();



            return View(zamestnanec);

        }

        public ActionResult DeleteZam(int id)
        {
            try
            {
                ZamestnanecDao zamDao = new ZamestnanecDao();
                Zamestnanec zam = zamDao.GetById(id);
                zamDao.Delete(zam);

                TempData["message-success"] = "Kniha byla smazana";

            }
            catch (Exception)
            {
                throw;
            }


            return RedirectToAction("Zamestnanci");


        }

        public ActionResult EditZam(int id)
        {

            ZamestnanecDao zamDao = new ZamestnanecDao();
            Zamestnanec zam = zamDao.GetById(id);

            PracovniPoziceDao poziceDao = new PracovniPoziceDao();
            PobockaDao pobockaDao = new PobockaDao();


            ViewBag.Pozice = poziceDao.GetAll();
            ViewBag.Pobocky = pobockaDao.GetAll();


            if (Request.IsAjaxRequest())
            {
                return PartialView(zam);

            }

            return View(zam);
        }

        public ActionResult UpdateZam(Zamestnanec zamestnanec, HttpPostedFileBase img)
        {

            ZamestnanecDao zamDao = new ZamestnanecDao();
            ImageUserDao imageDao = new ImageUserDao();
            ImageUser imageUser = imageDao.GetByZamestnanecId(zamestnanec.Id);
            if (imageUser == null)
            {
                imageUser = new ImageUser();
                Zamestnanec zamest = new Zamestnanec();
                zamest.Id = zamestnanec.Id;

                imageUser.Zamestnanec = zamest;
            }


            if (img != null)
            {

                if (img.ContentType == "image/jpeg" || img.ContentType == "image/png")
                {

                    Guid gu = Guid.NewGuid();
                    string imageName = gu.ToString() + ".jpg";
                    img.SaveAs(Server.MapPath("~/img/img_users/" + imageName));
                    if (imageUser.ImageName != null)
                    {
                        System.IO.File.Delete(Server.MapPath("~/img/img_users/" + imageUser.ImageName));
                        imageUser.ImageName = imageName;
                        imageDao.Update(imageUser);
                    }
                    else
                    {
                        imageUser.ImageName = imageName;
                        imageDao.Create(imageUser);

                    }

                }
            }


            try
            {


                zamDao.Update(zamestnanec);

                TempData["message-success"] = "Zaměstnanec byl editován";
            }
            catch (Exception)
            {
                throw;
            }

            return RedirectToAction("Zamestnanci");

        }

        public ActionResult ImageRenderZ(int id)
        {
            ImageUserDao imageDao = new ImageUserDao();
            ImageUser user = imageDao.GetByZamestnanecId(id);
            if (user == null)
            {
            user = new ImageUser();
            }

            return PartialView(user);
        }



        public ActionResult ImageRenderZak(int id)
        {
            ImageUserDao imageDao = new ImageUserDao();
            ImageUser user = imageDao.GetByZakaznikId(id);
            if (user == null)
            {
                user = new ImageUser();
            }

            return PartialView(user);
        }

   
        public ActionResult SearchCar(string phrase)
        {
            AutoDao autoDao = new AutoDao();
            IList<Auto> auta = autoDao.SearchCars(phrase);

            return View("Auta", auta);
        }
    }
}