﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GalleryController : Controller
    {
        // GET: Admin/Gallery
        public ActionResult Index()
        {
            return RedirectToAction("Automobil");
        }

        public ActionResult Automobil()
        {
            ImageAutoDao imageDao = new ImageAutoDao();
            IList<ImageAuto> imageAuto = imageDao.GetAll();



            return View(imageAuto);
            
        }




        public ActionResult Delete(int id)
        {
            try
            {
                ImageAutoDao imageDao = new ImageAutoDao();
                ImageAuto imageAuto = imageDao.GetById(id);

                imageDao.Delete(imageAuto);
                TempData["message-success"] = "Fotografie byla smazána";

            }
            catch (Exception)
            {
                throw;
            }

            return RedirectToAction("Automobil");

        }

        public ActionResult Add()
        {
            ViewBag.Auto = new AutoDao().GetAll();
            ImageAuto image = new ImageAuto();

            if (Request.IsAjaxRequest())
            {

                return PartialView(image);
            }

            return View(image);

        }




        public ActionResult Update(ImageAuto image, HttpPostedFileBase img)
        {

            ImageAutoDao imageDao = new ImageAutoDao();

            if (img != null)
            {
                if (img.ContentType == "image/jpeg" || img.ContentType == "image/png")
                {
                    Guid gu = Guid.NewGuid();
                    string imageName = gu.ToString() + ".jpg";
                    img.SaveAs(Server.MapPath("~/img/img_cars/" + imageName));
                    image.ImageName = imageName;
                    imageDao.Create(image);

                }
            }


            return RedirectToAction("Automobil");
        }

        public ActionResult SearchCar(string phrase)
        {


            AutoDao autoDao = new AutoDao();
            IList<Auto> auta = autoDao.SearchCars(phrase);


            ImageAutoDao imageDao = new ImageAutoDao();

            IList<ImageAuto> imageAuto = new List<ImageAuto>();

            foreach (var c in auta)
            {
                IList<ImageAuto> im = new List<ImageAuto>();
                im = imageDao.ImageAuto(c.Id);
                foreach (var images in im)
                {
                    imageAuto.Add(images);
                }


            }


            return View("Automobil", imageAuto);

        }

    }
}