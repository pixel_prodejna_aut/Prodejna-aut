﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataAccess.Dao;
using DataAccess.Model;
using Knihovna.Class;

namespace Knihovna.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminOptionsController : Controller
    {
        // GET: Admin/AdminOptions
        public ActionResult Index()
        {

            ImageUserDao imageUserDao = new ImageUserDao();

           

            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            ImageUser imageUser = imageUserDao.GetByZakaznikId(zakaznik.Id) ?? new ImageUser();

            ViewBag.Foto = imageUser.ImageName;
            return View(zakaznik);
        }

        [Authorize]
        public ActionResult UpdateO(HttpPostedFileBase img)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

            ImageUserDao imageDao = new ImageUserDao();
            ImageUser image = imageDao.GetByZakaznikId(zakaznik.Id);

            if (img != null)
            {
                if (img.ContentType == "image/jpeg" || img.ContentType == "image/png")
                {
                    Guid gu = Guid.NewGuid();
                    string imageName = gu.ToString() + ".jpg";
                    img.SaveAs(Server.MapPath("~/img/img_users/" + imageName));
                    if (image == null)
                    {
                        image = new ImageUser();
                        image.ImageName = imageName;
                        image.Zakaznik = zakaznik;
                        imageDao.Create(image);

                    }
                    else
                    {
                        image.ImageName = imageName;
                        imageDao.Update(image);

                    }



                }
            }


            return RedirectToAction("Index");
        }

        public ActionResult UpdateI(Zakaznik zakaznik)
        {
            try
            {

                ZakaznikDao zakaznikDao = new ZakaznikDao();
                zakaznikDao.Update(zakaznik);

                TempData["message-success"] = "Zákazník byl upraven";
            }
            catch (Exception)
            {
                throw;

            }



            return RedirectToAction("Index");
        }

        public ActionResult UpdateH(string PasswordOld, string PasswordNew)
        {

            MembershipProvider membership = new ProdejnaMembershipProvider();


            if (membership.ChangePassword(User.Identity.Name, PasswordOld, PasswordNew))
            {

                TempData["message-success"] = "Heslo bylo změněno";

            }
            else
            {
                TempData["message-success"] = "Heslo nebylo změněno";

            }

            return RedirectToAction("Index");
        }

        public ActionResult Fotog()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);

            ImageUserDao imageDao = new ImageUserDao();
            ImageUser image = imageDao.GetByZakaznikId(zakaznik.Id);
            if (image == null)
            {
                image = new ImageUser();
            }


            if (Request.IsAjaxRequest())
            {

                return PartialView(image);
            }

            return View(image);
        }

        public ActionResult Infos()
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(User.Identity.Name);
            if (Request.IsAjaxRequest())
            {

                return PartialView(zakaznik);
            }

            return View(zakaznik);
        }

        public ActionResult Heslo()
        {
            if (Request.IsAjaxRequest())
            {

                return PartialView();
            }

            return View();
        }
    }
}