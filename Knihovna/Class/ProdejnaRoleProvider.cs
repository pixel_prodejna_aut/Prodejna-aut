﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using DataAccess.Dao;
using DataAccess.Model;

namespace Knihovna.Class
{
    public class ProdejnaRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(username);

            if (zakaznik == null)
            {
                return false;

            }

            return zakaznik.Opravneni.Sopravneni == roleName;
        }

        public override string[] GetRolesForUser(string username)
        {
            ZakaznikDao zakaznikDao = new ZakaznikDao();
            Zakaznik zakaznik = zakaznikDao.GetByLogin(username);
            if (zakaznik == null)
            {
                return new string[] {};
            }

            return new[] {zakaznik.Opravneni.Sopravneni};
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}