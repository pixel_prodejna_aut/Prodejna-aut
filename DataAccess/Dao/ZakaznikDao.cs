﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class ZakaznikDao : DaoBase<Zakaznik>
    {

        public Zakaznik GetByLoginAndPassword(string login, string password)
        {
            return session.CreateCriteria<Zakaznik>().
                Add(Restrictions.Eq("Email", login)).
                Add(Restrictions.Eq("Password", password)).
                UniqueResult<Zakaznik>();

        }


        public Zakaznik GetByLogin(string login)
        {
            return session.CreateCriteria<Zakaznik>().
                Add(Restrictions.Eq("Email", login)).
                UniqueResult<Zakaznik>();

        }

        public IList<Zakaznik> GetListByLogin(string login)
        {
            return session.CreateCriteria<Zakaznik>()
                .Add(Restrictions.Like("Email", string.Format("%{0}%", login)))
                    .List<Zakaznik>();

        }
    }
}
