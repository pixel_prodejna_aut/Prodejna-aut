﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class ObjednavkaDao : DaoBase<Objednavka>
    {

        public IList<Objednavka> GetListByZakaznik(int id)
        {
            return session.CreateCriteria<Objednavka>()
                .Add(Restrictions.Eq("Zakaznik.Id", id))
                .List<Objednavka>();

        }
    }
}
