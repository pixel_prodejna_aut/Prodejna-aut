﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class ImageUserDao : DaoBase<ImageUser>
    {

        public ImageUser GetByZamestnanecId(int id)
        {

            return session.CreateCriteria<ImageUser>().Add(Restrictions.Eq("Zamestnanec.Id", id)).UniqueResult<ImageUser>();
        }


        public ImageUser GetByZakaznikId(int id)
        {

            return session.CreateCriteria<ImageUser>().Add(Restrictions.Eq("Zakaznik.Id", id)).UniqueResult<ImageUser>();
        }


    }
}
