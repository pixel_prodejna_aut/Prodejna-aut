﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Type;

namespace DataAccess.Dao
{
    public  class ImageAutoDao : DaoBase<ImageAuto>
    {
        public IList<ImageAuto> ImageAuto(int id)
        {
            return session.CreateCriteria<ImageAuto>().
                Add(Restrictions.Like("Auto.Id", id))
                .List<ImageAuto>();
        }

        public IList<ImageAuto> HeadImageAuto(int id)
        {
            return session.CreateCriteria<ImageAuto>()
                .Add(Restrictions.And(
                    Restrictions.Like("Auto.Id", id)
                    ,
                    Restrictions.Eq("SIsFirst", (bool)true)
                )).List<ImageAuto>();
        }

        public IList<ImageAuto> HeadImageAutoAll()
        {
            return session.CreateCriteria<ImageAuto>().Add(Restrictions.Eq("SIsFirst", (bool)true)).List<ImageAuto>();
        }


        


    }
}
