﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class TestovaciJizdaDao : DaoBase<TestovaciJizda>
    {
        public IList<TestovaciJizda> GetListByRegistrovany(int id)
        {
            return session.CreateCriteria<TestovaciJizda>()
                .Add(Restrictions.Eq("ZakaznikRegistrovany.Id", id))
                .List<TestovaciJizda>();

        }


    }
}
