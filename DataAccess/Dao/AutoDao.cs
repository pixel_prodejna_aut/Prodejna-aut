﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using DataAccess.Model;
using NHibernate.Criterion;
using NHibernate.Hql.Util;

namespace DataAccess.Dao
{
    public class AutoDao : DaoBase<Auto>
    {
        public IList<Auto> SearchCars(string phrase)
        {
            string[] words = phrase.Split(' ');

            if (words.Length>1)
            {
                return session.CreateCriteria<Auto>()
                    .CreateAlias("Znacka", "z")
                    .Add(Restrictions.Or(
                            Restrictions.Or(
                                Restrictions.Like("z.Sznacka", string.Format("%{0}%", words[1]))
                                ,
                                Restrictions.Like("Model", string.Format("%{0}%", words[0]))

                            ), Restrictions.And(
                                Restrictions.Like("z.Sznacka", string.Format("%{0}%", words[0]))
                                ,
                                Restrictions.Like("Model", string.Format("%{0}%", words[1]))

                            )
                        )
                    ).List<Auto>();
            }
            else
            {
                return session.CreateCriteria<Auto>()
                    .CreateAlias("Znacka", "z")
                    .Add(Restrictions.Or(
                                Restrictions.Like("z.Sznacka", string.Format("%{0}%", phrase))
                                ,
                                Restrictions.Like("Model", string.Format("%{0}%", phrase))
                            
                                )
                    ).List<Auto>();
            }

            
        }

        public IList<Auto> GetCarsPaged(int count, int page, out int totalCars)
        {
            totalCars = session.CreateCriteria<Auto>()
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return session.CreateCriteria<Auto>()
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Auto>();

        }






    }
}
