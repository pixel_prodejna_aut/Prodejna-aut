﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class ZakaznikRegistrovanyDao : DaoBase<ZakaznikRegistrovany>

    {

        public ZakaznikRegistrovany GetByZakaznik(int id)
        {

            return session.CreateCriteria<ZakaznikRegistrovany>().Add(Restrictions.Eq("Zakaznik.Id", id)).UniqueResult<ZakaznikRegistrovany>();
        }
    }
}
