﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class ZamestnanecDao : DaoBase<Zamestnanec>
    {

        public IList<Zamestnanec> GetByPobocka(int id)
        {

            return session.CreateCriteria<Zamestnanec>()
                .Add(Restrictions.Eq("Pobocka.Id", id)).List<Zamestnanec>();
        }

    }
}
