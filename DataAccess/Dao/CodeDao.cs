﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class CodeDao : DaoBase<Code>
    {


        public Code GetByCode(string code)
        {

            return session.CreateCriteria<Code>().Add(Restrictions.Like("Scode", code)).UniqueResult<Code>();
        }
    }
}
