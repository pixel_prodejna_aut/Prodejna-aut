﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class SystemsDao : DaoBase<Systems>
    {

        public IList<Systems> GetByMarker(int id)
        {

            return session.CreateCriteria<Systems>()
                .Add(Restrictions.Eq("Marker".ToString(), id)).List<Systems>();

        }
        
    }
}
