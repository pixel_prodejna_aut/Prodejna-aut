﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class MestoDao : DaoBase<Mesto>
    {

        public Mesto GetByPSC(string PSC)
        {

            return session.CreateCriteria<Mesto>().Add(Restrictions.Like("Psc", PSC)).UniqueResult<Mesto>();
        }
    }
}
