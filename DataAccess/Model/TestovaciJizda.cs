﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class TestovaciJizda : IEntity
    {
        public virtual int Id { get; set; }

        public virtual DateTime Datum { get; set; }
        public virtual int Delka { get; set; }
        public virtual string Popis { get; set; }
        public virtual Auto Auto { get; set; }
        public virtual ZakaznikRegistrovany ZakaznikRegistrovany { get; set; }
        public virtual Zamestnanec Zamestnanec { get; set; }


    }
}
