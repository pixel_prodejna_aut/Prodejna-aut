﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class ZakaznikRegistrovany : IEntity
    {
        public virtual int Id { get; set; }

        public virtual int PocetPixelFreeBodu { get; set; }
        public virtual string Telefon { get; set; }
        public virtual string Ulice { get; set; }
        public virtual string Mesto { get; set; }
        public virtual Zakaznik Zakaznik { get; set; }






    }
}
