﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class ImageUser : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string ImageName { get; set; }
        public virtual Zakaznik Zakaznik { get; set; }
        public virtual Zamestnanec Zamestnanec { get; set; }



    }
}
