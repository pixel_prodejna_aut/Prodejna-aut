﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Auto : IEntity
    {
        public virtual int Id { get; set; }

        public virtual string Barva { get; set; }
        public virtual int Cena { get; set; }
        public virtual int MaxRychlost { get; set; }
        public virtual string Model { get; set; }
        public virtual int Najeto { get; set; }
        public virtual string Palivo { get; set; }
        public virtual string Popis { get; set; }
        public virtual DateTime RokVyroby { get; set; }
        public virtual string TypKaroserie { get; set; }
        public virtual float Objem { get; set; }
        public virtual int Vykon { get; set; }
        public virtual Zeme Zeme { get; set; }
        public virtual Faktura Faktura { get; set; }
        public virtual Pobocka Pobocka { get; set; }
        public virtual Znacka Znacka { get; set; }

        public virtual string ModelAZnacka
        {
            get { return Znacka.Sznacka + " " + Model; }
        }


    }
}
