﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI.WebControls;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Zakaznik : MembershipUser, IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Pohlavi { get; set; }


       
        [Required(ErrorMessage = "Email je vyžadován")]
        public virtual string Email { get; set; }

        [Required(ErrorMessage = "Jméno je vyžadováno")]
        public virtual string Jmeno { get; set; }

        [Required(ErrorMessage = "Příjmení je vyžadováno")]
        public virtual string Prijmeni { get; set; }

       
        public virtual Mesto Psc { get; set; }

        public virtual Opravneni Opravneni { get; set; }



        [Required(ErrorMessage = "Heslo je vyžadováno")]
        [MinLength(5, ErrorMessage = "Zvolte bezpečnější heslo")]
        public virtual string Password { get; set; }


        public virtual string JmenoAPrijmeni
        {
            get { return Jmeno + " " + Prijmeni; }
        }

    }
}
