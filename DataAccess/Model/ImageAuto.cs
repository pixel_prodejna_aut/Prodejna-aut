﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class ImageAuto : IEntity
    {
        public virtual int Id { get; set; }

        public virtual string ImageName { get; set; }
        public virtual Auto Auto { get; set; }
        public virtual bool SIsFirst { get; set; }




    }
}
