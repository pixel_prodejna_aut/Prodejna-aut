﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Mesto : IEntity
    {
        public virtual int Id { get; set; }


        [RegularExpression("^[0-9]*$")]
        [Required(ErrorMessage = "PSČ je vyžadováno")]
        [MinLength(5, ErrorMessage = "Zadejte PSČ")]
        [MaxLength(5, ErrorMessage = "Zadejte PSČ")]
        public virtual string Psc { get; set; }
    }
}
