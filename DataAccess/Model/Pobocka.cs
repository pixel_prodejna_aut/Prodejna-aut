﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Pobocka : IEntity

    {
        public virtual int Id { get; set; }


        [RegularExpression("^[0-9]*$")]
        [Required(ErrorMessage = "PSČ je vyžadováno")]
        public virtual string CelkovaKapacita { get; set; }


        [Required(ErrorMessage = "PSČ je vyžadováno")]
        public virtual string Cp { get; set; }

        [RegularExpression("^[0-9]*$")]
        [Required(ErrorMessage = "PSČ je vyžadováno")]
        public virtual string PocetVolnychMist { get; set; }

        [MinLength(5,ErrorMessage = "Příliš krátký název")]
        public virtual string Nazev { get; set; }


        public virtual Region Region { get; set; }








    }
}
