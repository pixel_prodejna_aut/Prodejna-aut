﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Objednavka : IEntity
    {
        public virtual int Id { get; set; }

        public virtual DateTime Datum { get; set; }
        public virtual Zamestnanec Zamestnanec { get; set; }
        public virtual Zakaznik Zakaznik { get; set; }
        public virtual Prislusenstvi Prislusenstvi { get; set; }
        public virtual Auto Auto { get; set; }




    }
}
