﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Zamestnanec : IEntity
    {
        public virtual int Id { get; set; }

        public virtual string PocetProdanychAut { get; set; }
        public virtual string Jmeno { get; set; }
        public virtual string Plat { get; set; }
        public virtual string Prijmeni { get; set; }
        public virtual string Titul { get; set; }
        public virtual int Psc { get; set; }
        public virtual Pobocka Pobocka { get; set; }
        public virtual PracovniPozice PracovniPozice { get; set; }

        public virtual string JmenoAPrijmeni
        {
            get { return Jmeno + " " + Prijmeni; }}

    }
}
