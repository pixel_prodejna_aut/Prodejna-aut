﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Systems : IEntity
    {
        public virtual int Id { get; set; }
        public virtual int Marker { get; set; }
        public virtual string Value { get; set; }
        public virtual string Description { get; set; }

    }
}
