﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Prislusenstvi : IEntity
    {
        public virtual int Id { get; set; }

        [MinLength(3, ErrorMessage = "Příliš krátký název")]
        public virtual string Nazev { get; set; }


        [MinLength(30, ErrorMessage = "Příliš krátký popis")]
        public virtual string Popis { get; set; }
    }
}
